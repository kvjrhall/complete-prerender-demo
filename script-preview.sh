#!/bin/bash
set -o errexit -o pipefail -o noclobber -o nounset

# Relevant Script Directories
# -----------------------------------------------------------------------------
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


# Logging boilerplate
# -----------------------------------------------------------------------------
function loginfo {
  echo -e "\e[33m\e[1m[INFO] ($(basename $0)) $1\e[0m"
}

function heading_1 {
    echo
    loginfo "$1"
    echo -e "\e[33m\e[1m[    ] ($(basename $0)) ================================================================================\e[0m"
}

function heading_2 {
    echo
    loginfo "$1"
    echo -e "\e[33m\e[1m[    ] ($(basename $0)) --------------------------------------------------------------------------------\e[0m"
}

function on_fail {
  echo >&2
  echo -e "\e[33m\e[1m[    ] ($(basename $0)) ================================================================================\e[0m" >&2
  echo -e "\e[33m\e[1m[FAIL] ($(basename $0)) $1\e[0m" >&2
  echo -e "\e[33m\e[1m[    ] ($(basename $0)) ================================================================================\e[0m" >&2
  exit 1
}


function python_serve {
  # Server Configuration
  # ----------------------------------------------------------------------------
  HTML_DIR="$DIR/dist/complete-prerender-demo/browser"
  if [ $# -gt 0 ]; then
      HTML_DIR="$1"
  fi
  loginfo "Serving from: ${HTML_DIR}"

  PORT=1234
  if [ $# -gt 1 ]; then
      PORT="$2"
  fi
  loginfo "Using port $PORT"


  # Python Environment
  # -----------------------------------------------------------------------------
  ENV_DIR="$DIR/.envs/html"
  loginfo "Using python directory: ${ENV_DIR}"
  source "$ENV_DIR/bin/activate"


# cat > /tmp/python_serve.py << 'TEMP_SCRIPT'
# #!/usr/bin/env python3
#
# import sys
# from http.server import HTTPServer, BaseHTTPRequestHandler
#
# if len(sys.argv)-1 != 2:
#     print("""
# Usage: {} <port_number> <url>
#     """.format(sys.argv[0]))
#     sys.exit()
#
# class Redirect(BaseHTTPRequestHandler):
#    def do_GET(self):
#        self.send_response(302)
#        self.send_header('Location', sys.argv[2])
#        self.end_headers()
#
# HTTPServer(("", int(sys.argv[1])), Redirect).serve_forever()
# TEMP_SCRIPT


  # Launch Local Preview
  # -----------------------------------------------------------------------------
  cd "$HTML_DIR"
  python -m http.server $PORT
  # python /tmp/python_serve.py $PORT
}

rm -rf dist/
npm run prerender
python_serve
