import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ViewData } from './demonstration-resolver.service';

@Component({
  selector: 'app-demonstration',
  templateUrl: './demonstration.component.html',
  styleUrls: ['./demonstration.component.scss'],
})
export class DemonstrationComponent implements OnInit, AfterViewInit {
  constructor(private readonly route: ActivatedRoute) {}

  jsonMessage?: string;

  ngOnInit(): void {
    console.log('DemonstrationComponent', 'ngOnInit');
    this.route.data.subscribe((data) => {
      console.log('DemonstrationComponent', 'data.subscribe');
      const viewData: ViewData = data['viewData'];
      console.log('DemonstrationComponent', 'viewData = ', viewData);
      this.jsonMessage = viewData.jsonMessage;
      console.log('DemonstrationComponent', 'data.subscribe done');
    });
  }

  ngAfterViewInit(): void {
    console.log('DemonstrationComponent', 'ngAfterViewInit');
  }
}
