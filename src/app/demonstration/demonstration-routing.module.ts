import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemonstrationResolverService } from './demonstration-resolver.service';
import { DemonstrationComponent } from './demonstration.component';

const routes: Routes = [
  {
    path: '',
    component: DemonstrationComponent,
    resolve: {
      viewData: DemonstrationResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DemonstrationRoutingModule {}
