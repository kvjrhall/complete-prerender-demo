import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { map, Observable } from 'rxjs';
import { DemonstrationService } from '../demonstration-service/demonstration.service';

export interface ViewData {
  jsonMessage: string;
}

/**
 * Prerendering should [1] wait for data before rendering view, which we
 * accomplish by [2] using a view resolver to fetch the data before view
 * rendering begins.
 *
 * https://stackoverflow.com/questions/49504266/wait-for-data-before-rendering-view-in-angular-5
 * https://angular.io/guide/router-tutorial-toh#fetch-data-before-navigating
 */
@Injectable({
  providedIn: 'root',
})
export class DemonstrationResolverService implements Resolve<ViewData> {
  constructor(private readonly demoService: DemonstrationService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ViewData> {
    console.log('DemonstrationResolverService', 'resolve');
    const jsonData$ = this.demoService.getJson('1');
    return jsonData$.pipe(
      map((jsonData) => {
        console.log('DemonstrationResolverService', 'jsonData = ', jsonData);
        return { jsonMessage: jsonData.message };
      })
    );
  }
}
