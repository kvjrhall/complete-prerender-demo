import { TestBed } from '@angular/core/testing';
import { AssetLoader, ASSET_LOADER } from '../asset-loader/asset-loader';
import { DemonstrationService } from './demonstration.service';

describe('DemonstrationService', () => {
  let service: DemonstrationService;
  let loader: AssetLoader;

  beforeEach(() => {
    loader = jasmine.createSpyObj('AssetLoader', ['getAsset']);
    TestBed.configureTestingModule({
      providers: [{ provide: ASSET_LOADER, useValue: loader }],
    });
    service = TestBed.inject(DemonstrationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
