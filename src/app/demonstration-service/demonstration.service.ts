import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AssetLoader, ASSET_LOADER } from '../asset-loader/asset-loader';

export interface JsonExample {
  message: string;
}

@Injectable({
  providedIn: 'root',
})
export class DemonstrationService {
  constructor(
    @Inject(ASSET_LOADER) private readonly assetLoader: AssetLoader
  ) {}

  getJson(id: string): Observable<JsonExample> {
    console.log('DemonstrationService', 'getJson', 'id = ', id);
    const asset = this.assetLoader.getAsset<JsonExample>(id + '.example.json');
    console.log('DemonstrationService', 'getJson', 'returning asset');
    return asset;
  }
}
