import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ASSET_LOADER } from './asset-loader/asset-loader';
import { BrowserAssetLoader } from './asset-loader/browser-asset-loader';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [
    {
      provide: ASSET_LOADER,
      useClass: BrowserAssetLoader,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
