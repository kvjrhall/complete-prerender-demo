Why this project?

1. Tutorials for Angular pre-rendering and server-side rendering are almost all
   add-ridden duplications of the official documentation with a focus on selling
   you their product. They are useless and they waste hours of time by
   preventing anybody from finding a useful minimal example.

2. I think that my use case is a common one and that people may benefit from a
   functional minimal example. I generate a lot of my content through small
   demonstrations. I'd like to drop that content into CDN/static assets, but
   still get search engine optimization and still have that content feel like a
   cohesive part of my website. For this to work I need to load static and
   dynamic resources which must be in the rendered results.

3. At this time the officially- and community-prescribed solutions are vague,
   conflicting, and threads usually end with someone saying
   ["nvm, I figured it out" without elaborating](https://xkcd.com/979/). I
   don't want others to suffer the frustration that I did.

4. Myself or other may wish to clone this repository so that they can get a
   quick-start on a new angular project that needs to have server-side
   rendering.

# Steps Followed

This repository will certainly become out-of-date as the angular ecosystem
evolves and its value as an informative resource will diminish. What may be
more useful in the longer term is the steps taken and the rationale applied.
Hopefully the procedure can then (1) be adapted to API changes and (2) help to
debug problems that may still be easy to encounter.

1. `ng new your-project-name`
2. `ng add @nguniversal/express-engine` [[1](https://angular.io/guide/universal#universal-tutorial)]
3. `AssetLoader` - Construct two services with an injection token so that one
   can be given in the browser and another can be given on the server. This allows
   us to deal with the inconsistencies of server-side-rendering versus browser
   rendering. For example, the availability of `DOMParser` in browser and our need
   to replace that with `domino` on the server. Second example, though `HttpClient`
   [[2](https://angular.io/guide/http)]
   should be able to resolve static assets, it seems to consistently fail during
   `npm run prerender` even if it works during `npm run dev:ssr`. The official way
   to deal with this is to construct and absolute URI to reference the asset
   [[3](https://angular.io/guide/universal#using-absolute-urls-for-http-data-requests-on-the-server),
   [4](https://github.com/angular/universal/issues/858),
   [5](https://stackoverflow.com/a/58394725)], but
   no documentation or example found so far shows a method to resolve the `port`
   that the prerendering server is using for such connections. My assumption is
   that this is resolved in the enterprise environment by pointing the prerenderer
   at the organization's CDN. For me, it's easier to simply use the `fs` module
   that we have available through node. Tree shaking ensures that the node
   dependencies introduced through `ServerAssetLoader` do not leak into the browser
   application code.
4. `DemonstrationService`
5. `DemonstrationResolverService` - If we perform the service request in the
   view then the view is rendered without that initial data
   [[6](https://stackoverflow.com/a/49504640)]. This is then present during
   prerender and ssr. If we add a
   resolver into the mix, the view will not begin to render until the data has
   been loaded which will ensure that prerender and ssr will yield a populated
   page [[6](https://stackoverflow.com/a/49504640)]. If we were to host the page
   without SSR or prerendering, the resolver logic could have the effect of causing
   a noticeable lag prior to first paint [[7](https://angular.io/guide/router-tutorial-toh#fetch-data-before-navigating)],
   but that shouldn't be a problem for us.
6. `DemonstrationComponent` - Make no asynchronous requests and retrieve data
   by subscribing to the `ActivatedRoute` object. (**TODO** look into the significance
   of using the `data.snapshot` subscription for my own education. It does not
   appear to be necessary for this methodology.)

# Conclusion

Steps 3, 5, and 6 above are the crucial parts above but they should be fairly
encapsulated. If you're not struggling with getting static assets through
`HttpClient` during prerender / ssr, then ignore step 3 and use your own
methods. Your app should behave consistently whether you use `npm run start`,
`npm run dev:ssr` or `npm run prerender` (followed by serving the static content
from your `./dist/*/browser` folder).

# Possible Enhancements

This project could serve as a more comprehensive example for this use case if I
were to introduce the following enhancements:

1. Loading and parsing html content from assets with `domino` during the SSR
   process.
2. Fleshing out the demonstration to look more like a blog, including the usage
   of an `ArticleService`, `ArticleListComponent`, and `ArticleDetailComponent`.
3. Demonstrate SEO tagging in an `ArticleDetailComponent` using data from
   `ArticleService`
4. Use a node-native http server and drop my usage of python. The two scripts
   `script-setup-dev.sh` and `script-preview.sh` were lazily imported from other
   projects so that I could simply ignore it and move on. I believe this is
   embarrassingly easy to accomplish.
